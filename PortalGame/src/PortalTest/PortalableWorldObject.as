/**
 * Created with IntelliJ IDEA.
 * User: $
 * Date: 18/05/13
 * Time: 18:41
 * To change this template use File | Settings | File Templates.
 */
package PortalTest {

import Box2D.Collision.b2WorldManifold;
import Box2D.Common.Math.b2Vec2;
import Box2D.Dynamics.Contacts.b2Contact;
import Box2D.Dynamics.b2Body;
import Box2D.Dynamics.b2ContactImpulse;

import Game2D.ContactPostSolveEvent;
import Game2D.ContactPreSolveEvent;
import Game2D.Point2D;
import Game2D.WorldObject;

public class PortalableWorldObject extends WorldObject {

    private var portalsTouching:Vector.<Portal> = new Vector.<Portal>();

    public function PortalableWorldObject(body:b2Body) {
        super(body);
        addEventListener(ContactPreSolveEvent.PRE_SOLVE,onPreSolve);
        addEventListener(ContactPostSolveEvent.POST_SOLVE, onPostSolve);
    }

    private function onPostSolve(event:ContactPostSolveEvent):void {
        if(portalsTouching.length>0)
        {
            var worldManifold:b2WorldManifold = new b2WorldManifold();
            event.contact.GetWorldManifold(worldManifold);
            for(var i:int = 0; i<portalsTouching.length;i++)
            {
                var curPortal:Portal = portalsTouching[i];

                var otherBody:b2Body = curPortal.GetClone(body);

                if(otherBody)
                {
                    CopyImpulses(curPortal, otherBody, event.contact, event.impulse);
                }
            }
        }
    }

    private function onPreSolve(event:ContactPreSolveEvent):void {
        if(portalsTouching.length>0)
        {
            var worldManifold:b2WorldManifold = new b2WorldManifold();
            event.contact.GetWorldManifold(worldManifold);
            for(var i:int = 0; i<portalsTouching.length;i++)
            {
                var curPortal:Portal = portalsTouching[i];

                var cloneBody:BodyClone = event.otherFixture.GetBody().GetUserData() as BodyClone;
                if(cloneBody&&cloneBody.originalBody==body)
                    continue;
                var cloneSelf:BodyClone = this as BodyClone;
                if(cloneSelf&&cloneSelf.originalBody==event.otherFixture.GetBody())
                    continue;
                //if(event.otherFixture.GetBody().GetUserData()!=curPortal)
                {
                    if(curPortal.body.GetLocalPoint(worldManifold.points[0]).y<0)
                        event.contact.SetEnabled(false);
                    //trace(curPortal.body.GetLocalPoint(worldManifold.points[0]).y);
                    //if(curPortal.body.GetLocalPoint(worldManifold.points[0]).y)
                    //trace("eek!");
                }
            }
        }
    }


    protected final function CopyImpulses(curPortal:Portal, otherBody:b2Body, contact:b2Contact, impulse:b2ContactImpulse):void {

        var worldManifold:b2WorldManifold = new b2WorldManifold();
        contact.GetWorldManifold(worldManifold);

        var massRatio:Number = otherBody.GetMass()/body.GetMass();

        var c:Boolean = contact.GetFixtureA().GetBody()==body;
        var pointCount:uint = contact.GetManifold().pointCount;
        for(var j:uint = 0; j<pointCount; j++)
        {
            var otherImpulsePoint:Point2D = curPortal.PortalPoint(Point2D.FromVec(worldManifold.points[j]));
            var normalVector:Point2D =      curPortal.PortalVector(Point2D.FromVec(worldManifold.normal), true);
            if(c)
            {
                //trace("inverted!!!!");
                normalVector = normalVector.Times(-1);
            }

            var normalForce:Point2D = normalVector.Times(impulse.normalImpulses[j]*massRatio);
            var tangentForce:Point2D = normalVector.tangentLeft.Times(impulse.tangentImpulses[j]*massRatio);

            //if(a)
            //tangentForce = tangentForce.tangentLeft;
            //else
            //    tangentForce = tangentForce.tangentRight;


            //otherBody.ApplyImpulse(normalForce.vec,otherImpulsePoint.vec);
            //otherBody.ApplyImpulse(tangentForce.vec,otherImpulsePoint.vec);
            otherBody.ApplyImpulse(normalForce.Add(tangentForce).vec,otherImpulsePoint.vec);

//            if(false)
//            {
//                var gfx:Graphics = Portals.debugSprite.graphics;
//
//                var displayPoint:Point2D = otherImpulsePoint.Times(Portals.physicsScale);
//                var forcePoint:Point2D = displayPoint.Add(normalForce.Times(Portals.physicsScale*10));
//
//                gfx.lineStyle(1,Color.Red);
//                gfx.moveTo(displayPoint.x,displayPoint.y);
//                gfx.lineTo(forcePoint.x, forcePoint.y);
//                forcePoint = displayPoint.Add(tangentForce.Times(Portals.physicsScale*10));
//                gfx.lineStyle(1,Color.Green);
//                gfx.moveTo(displayPoint.x,displayPoint.y);
//                gfx.lineTo(forcePoint.x, forcePoint.y);
//
//                displayPoint = Point2D.FromVec(worldManifold.points[j]).Times(Portals.physicsScale);
//                forcePoint = displayPoint.Add(Point2D.FromVec(worldManifold.normal).Times(impulse.normalImpulses[j]*10*Portals.physicsScale));
//                gfx.lineStyle(1,Color.Blue);
//                gfx.moveTo(displayPoint.x,displayPoint.y);
//                gfx.lineTo(forcePoint.x, forcePoint.y);
//                forcePoint = displayPoint.Add(Point2D.FromVec(worldManifold.normal).tangentLeft.Times(impulse.tangentImpulses[j]*10*Portals.physicsScale));
//                gfx.lineStyle(1,Color.Black);
//                gfx.moveTo(displayPoint.x,displayPoint.y);
//                gfx.lineTo(forcePoint.x, forcePoint.y);
//            }
        }
    }


    public function TouchingPortal(portal:Portal, val:Boolean):void {
        if(val) // Started touching!
        {
            if(portalsTouching.indexOf(portal)==-1)
                portalsTouching.push(portal);
        }
        else { // Stopped touching!
            portalsTouching.splice(portalsTouching.indexOf(portal),1);
        }
    }

    public function ApplyTorque(torque:Number):void
    {
        body.ApplyTorque(torque);

        if(portalsTouching.length>0)
        {
            for(var i:int = 0; i<portalsTouching.length;i++)
            {
                var curPortal:Portal = portalsTouching[i];

                var otherBody:b2Body = curPortal.GetClone(body);
                if(otherBody)
                {

                    otherBody.ApplyTorque(torque*otherBody.GetInertia()/body.GetInertia());
                }
            }
        }
    }

    public function ApplyForce(force:b2Vec2, point:b2Vec2):void {
        body.ApplyForce(force, point);

        if(portalsTouching.length>0)
        {
            for(var i:int = 0; i<portalsTouching.length;i++)
            {
                var curPortal:Portal = portalsTouching[i];

                var otherBody:b2Body = curPortal.GetClone(body);
                if(otherBody)
                {
                    var otherPoint:b2Vec2 = curPortal.PortalPoint(Point2D.FromVec(point)).vec;////otherBody.GetWorldPoint(body.GetLocalPoint(point));
                    var otherForce:b2Vec2 = curPortal.PortalVector(Point2D.FromVec(force),true).
                            Times(otherBody.GetMass()/body.GetMass()).vec;//otherBody.GetWorldVector(body.GetLocalVector(force));
                    otherBody.ApplyForce(otherForce,otherPoint);

                    //curPortal.PortalBody(body, otherBody);

//                    if(false)
//                    {
//                        var a:Point2D = Point2D.FromVec(otherPoint).Times(Portals.physicsScale);
//                        var b:Point2D = Point2D.FromVec(otherForce).Times(Portals.physicsScale);
//                        var c:Point2D = Point2D.FromVec(point).Times(Portals.physicsScale);
//                        var d:Point2D = Point2D.FromVec(force).Times(Portals.physicsScale);
//
//                        Portals.debugSprite.graphics.lineStyle(2,Color.Green);
//                        Portals.debugSprite.graphics.moveTo(a.x, a.y);
//                        Portals.debugSprite.graphics.lineTo(a.x+b.x, a.y+b.y);
//                        Portals.debugSprite.graphics.moveTo(c.x, c.y);
//                        Portals.debugSprite.graphics.lineTo(c.x+d.x, c.y+d.y);
//                    }
                }
            }
        }
    }

    public override function ApplyImpulse(impulse:b2Vec2, point:b2Vec2):void {
        super.ApplyImpulse(impulse, point);

        if(portalsTouching.length>0)
        {
            for(var i:int = 0; i<portalsTouching.length;i++)
            {
                var curPortal:Portal = portalsTouching[i];

                var otherBody:b2Body = curPortal.GetClone(body);
                if(otherBody)
                {
                    var otherPoint:b2Vec2 = curPortal.PortalPoint(Point2D.FromVec(point)).vec;////otherBody.GetWorldPoint(body.GetLocalPoint(point));
                    var otherImpulse:b2Vec2 = curPortal.PortalVector(Point2D.FromVec(impulse),true)
                            .Times(otherBody.GetMass()/body.GetMass()).vec;//otherBody.GetWorldVector(body.GetLocalVector(force));
                    otherBody.ApplyImpulse(otherImpulse,otherPoint);
                }
            }
        }
    }

    override public function SetAngularVelocity(vel:Number): void {
        super.SetAngularVelocity(vel);

        if(portalsTouching.length>0)
        {
            for(var i:int = 0; i<portalsTouching.length;i++)
            {
                var curPortal:Portal = portalsTouching[i];

                var otherBody:b2Body = curPortal.GetClone(body);
                if(otherBody)
                {
                    if(!otherBody.IsAwake())
                        otherBody.SetAwake(true);
                    otherBody.SetAngularVelocity(vel);
                }
            }
        }
    }

    override public function SetLinearVelocity(vel:Point2D): void{
        super.SetLinearVelocity(vel);

        if(portalsTouching.length>0)
        {
            for(var i:int = 0; i<portalsTouching.length;i++)
            {
                var curPortal:Portal = portalsTouching[i];

                var otherBody:b2Body = curPortal.GetClone(body);
                if(otherBody)
                {
                    if(!otherBody.IsAwake())
                        otherBody.SetAwake(true);
                    //curPortal.PortalBody(body, otherBody);
                    otherBody.SetLinearVelocity(curPortal.PortalVector(vel, true).vec);
                }
            }
        }
    }
}
}
