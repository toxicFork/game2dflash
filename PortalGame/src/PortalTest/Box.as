/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 6/6/13
 * Time: 2:44 PM
 */
package PortalTest {
import Box2D.Collision.Shapes.b2PolygonShape;
import Box2D.Dynamics.b2Body;
import Box2D.Dynamics.b2BodyDef;
import Box2D.Dynamics.b2FixtureDef;
import Box2D.Dynamics.b2World;

import Game2D.Game;

import Game2D.Input;
import Game2D.Point2D;

import flash.display.Bitmap;

import flash.display.Sprite;

import flash.ui.Keyboard;

public class Box extends PortalableWorldObject {

    [Embed(source="../../images/crate.jpg")]
    public static var crate:Class;

    //public static const boxImage:Bitmap = new Player.imageClass();

    public function Box(world:b2World) {
        var bodyDef:b2BodyDef = new b2BodyDef();
        bodyDef.type = b2Body.b2_dynamicBody;
        super(world.CreateBody(bodyDef));

        image = new crate();
        var bmp:Bitmap = image as Bitmap;
        imageOffset = new Point2D(-bmp.width/2, -bmp.height/2);
        imageScale = new Point2D(1/bmp.width,1/bmp.height);

        var fixtureDef:b2FixtureDef = new b2FixtureDef();
        fixtureDef.density = 1.0;
        var shapeDef:b2PolygonShape = new b2PolygonShape();
        if(Input.keyDown(Keyboard.SHIFT))
        {
            //imageScale = new Point2D(200/8, 5/8);
            imageScale = new Point2D(2/bmp.width,1/bmp.height);
            shapeDef.SetAsBox(1, 0.5);
        }
        else
        {
            shapeDef.SetAsBox(0.5, 0.5);
        }
        fixtureDef.shape = shapeDef;
        body.CreateFixture(fixtureDef);

        if(Input.keyDown(Keyboard.SHIFT))
        {
            body.SetGravityScale(0);
            body.SetAngularDamping(0.5);
            body.SetLinearDamping(0.5);
        }
    }
}
}
