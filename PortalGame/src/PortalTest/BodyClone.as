/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 5/30/13
 * Time: 4:19 PM
 * To change this template use File | Settings | File Templates.
 */
package PortalTest {

import Box2D.Common.Math.b2Vec2;
import Box2D.Dynamics.b2Body;

import Game2D.ContactPostSolveEvent;
import Game2D.WorldObject;

public class BodyClone extends PortalableWorldObject {
    public var originalBody:b2Body;
    public var portal:Portal;
    public function BodyClone(body:b2Body, originalBody:b2Body, portal:Portal) {
        super(body);
        this.originalBody = originalBody;
        this.portal = portal;
        var otherObject:WorldObject = originalBody.GetUserData() as WorldObject;
        if(otherObject)
        {
            this.scale = otherObject.scale;
            this.image = otherObject.image;
            this.imageOffset = otherObject.imageOffset;
            this.imageScale = otherObject.imageScale;
        }

        addEventListener(ContactPostSolveEvent.POST_SOLVE,onPostSolve);
    }

    override public function update(deltaTime:Number):void {

    }

    override public function ApplyImpulse(impulse:b2Vec2, point:b2Vec2):void
    {
        super.ApplyImpulse(impulse, point);

        var originalPoint:b2Vec2 = originalBody.GetWorldPoint(body.GetLocalPoint(point));
        var originalImpulse:b2Vec2 = originalBody.GetWorldVector(body.GetLocalVector(impulse));
        originalBody.ApplyImpulse(originalImpulse,originalPoint);
    }

    private function onPostSolve(event:ContactPostSolveEvent):void
    {
        CopyImpulses(portal, originalBody, event.contact, event.impulse);
    }
}
}
