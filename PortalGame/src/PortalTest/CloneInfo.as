/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 5/29/13
 * Time: 7:04 AM
 * To change this template use File | Settings | File Templates.
 */
package PortalTest {

import Box2D.Common.Math.b2Vec2;
import Box2D.Dynamics.b2Body;

public class CloneInfo {
    public var contactCount:int;
    public var originalBody:b2Body;
    private var _cloneBody:BodyClone;
    public var lastPos:b2Vec2;
    private var portal:Portal;
    public function CloneInfo(body:b2Body, portal:Portal) {
        contactCount = 1;
        originalBody = body;
        this.portal = portal;
    }

    public function get cloneBody():b2Body{
        if(_cloneBody==null)
            return null;
        return _cloneBody.body;
    }

    public function set cloneBody(value:b2Body):void {
        _cloneBody = new BodyClone(value,originalBody,portal);
    }
}
}
