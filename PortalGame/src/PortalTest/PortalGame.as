/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 6/2/13
 * Time: 10:34 AM
 * To change this template use File | Settings | File Templates.
 */
package PortalTest {
import Box2D.Collision.Shapes.b2PolygonShape;
import Box2D.Dynamics.b2Body;
import Box2D.Dynamics.b2BodyDef;
import Box2D.Dynamics.b2FixtureDef;

import Game2D.*;

import flash.events.Event;
import flash.events.KeyboardEvent;

import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.text.TextField;
import flash.ui.Keyboard;
import flash.utils.Timer;

[SWF (frameRate="60")]
public class PortalGame extends Game {
    private var player:Player;
    private var tf:TextField = new TextField();

    public function PortalGame() {
        addEventListener(GameEvent.START,start);
        super();

        addEventListener(Event.ENTER_FRAME, enterFrame);
        var timer:Timer = new Timer(1000);
        timer.addEventListener(TimerEvent.TIMER, timerHit);
        timer.start();

        stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDown);
    }

    private function keyDown(event:KeyboardEvent):void {
        if(event.keyCode==Keyboard.R)
        {
            rendering = !rendering;
        }
        if(event.keyCode==Keyboard.F)
        {
            debug = !debug;
        }
    }

    private var frames:int = 0;
    private function timerHit(event:TimerEvent):void {
        tf.text = "FPS: "+frames;
        frames = 0;
    }

    private function enterFrame(event:Event):void {
        frames++;
    }

    private function start(event:GameEvent):void {
        addChild(tf);

        var wall:b2PolygonShape= new b2PolygonShape();
        var wallBd:b2BodyDef = new b2BodyDef();
        var wallB:b2Body;

        // Left
        wallBd.position.Set( -95 / physicsScale, 360 / physicsScale / 2);
        wall.SetAsBox(100/physicsScale, 400/physicsScale/2);
        wallB = world.CreateBody(wallBd);
        wallB.CreateFixture2(wall);

        // Right
        wallBd.position.Set((640 + 95) / physicsScale, 360 / physicsScale / 2);
        wallB = world.CreateBody(wallBd);
        wallB.CreateFixture2(wall);

        // Top
        wallBd.position.Set(640 / physicsScale / 2, -95 / physicsScale);
        wall.SetAsBox(680/physicsScale/2, 100/physicsScale);
        wallB = world.CreateBody(wallBd);
        wallB.CreateFixture2(wall);

        // Bottom
        wallBd.position.Set(640 / physicsScale / 2, (360 + 95) / physicsScale);
        wallB = world.CreateBody(wallBd);
        wallB.CreateFixture2(wall);

        var a:Portal = new Portal(100/physicsScale,world,drawSprite);
        var b:Portal = new Portal(75/physicsScale,world,drawSprite);

        a.otherPortal = b;
        b.otherPortal = a;


        a.rot = Math.PI*0.5;
        a.pos = new Point2D(200/physicsScale+320/physicsScale,200/physicsScale);

        b.pos = new Point2D(200/physicsScale-20/physicsScale,200/physicsScale);
        b.rot = -Math.PI*0.5;
        //objects.push(a);
        //objects.push(b);

        player = new Player(world);
        player.pos = new Point2D(400/physicsScale,300/physicsScale);

        stage.addEventListener(MouseEvent.RIGHT_CLICK, rightClick);
        //stage.addEventListener(MouseEvent.MIDDLE_CLICK, middleClick);
    }

    private function rightClick(event:MouseEvent):void {
        var worldObject:WorldObject;

        if(event.altKey)
        {
            trace("!!?");
            var body:b2Body = getBodyAtMouse();
            if(body==null)
                return;
            if(!(body.GetUserData() as Player))
            {
                if((worldObject = body.GetUserData() as WorldObject)!=null)
                {
                    worldObject.Destroyed();
                }
                world.DestroyBody(body);
            }
            debugSprite.graphics.clear();
            return;
        }
        new Box(world).pos = mouseCoords.Times(1/physicsScale);
    }

    override protected function updateView():void{
        if(!(player&&view))
        {
            trace("wat");
            return;
        }
        view.position = player.pos.Clone();
        view.rect.x = view.position.x-view.rect.width/2;
        view.rect.y = view.position.y-view.rect.height/2;
        view.rotation = player.rot;
        view.scale = player.scale;
    }
}
}
