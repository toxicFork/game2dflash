/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 5/28/13
 * Time: 9:13 PM
 * To change this template use File | Settings | File Templates.
 */
package PortalTest {

import Box2D.Collision.Shapes.b2CircleShape;
import Box2D.Collision.Shapes.b2PolygonShape;
import Box2D.Common.Math.b2Vec2;
import Box2D.Dynamics.b2Body;
import Box2D.Dynamics.b2BodyDef;
import Box2D.Dynamics.b2FixtureDef;
import Box2D.Dynamics.b2World;

import Game2D.Color;

import Game2D.Functions.Deg2Rad;
import Game2D.Game;
import Game2D.Input;
import Game2D.Point2D;

import flash.display.Sprite;

import flash.ui.Keyboard;

public class Player extends PortalableWorldObject {
    private const length:Number = 0.5;
    private const headSize:Number = 0.125;
    private const widthRatio:Number = 0.1;

    public function Player(world:b2World) {
        var bodyDef:b2BodyDef = new b2BodyDef();
        bodyDef.type = b2Body.b2_dynamicBody;
        super(world.CreateBody(bodyDef));

        image = new Sprite();
        var imageSprite:Sprite = image as Sprite;
        imageSprite.graphics.lineStyle(1/Game.physicsScale,Color.Green);
        imageSprite.graphics.drawCircle(-(length+headSize*0.5*length)*1,0, headSize*1);
        imageSprite.graphics.drawRect(-length*1,-widthRatio*length*1,
                length*1*2,widthRatio*length*1*2);
        imageOffset = new Point2D(0, 0);
        imageScale = new Point2D(1, 1);

        var fixtureDef:b2FixtureDef = new b2FixtureDef();
        fixtureDef.density = 1.0;
        var shapeDef:b2PolygonShape = new b2PolygonShape();
        shapeDef.SetAsBox(length,widthRatio*length);
        fixtureDef.shape = shapeDef;
        body.CreateFixture(fixtureDef);
        fixtureDef = new b2FixtureDef();
        fixtureDef.density = 1.0;
        shapeDef = new b2PolygonShape();
        var circleShape:b2CircleShape = new b2CircleShape(headSize);
        circleShape.SetLocalPosition(new b2Vec2(-(length+headSize*0.5*length),0));
        //shapeDef.SetAsOrientedBox(length*headSize,length*headSize,new b2Vec2(-(length+headSize*length),0));
        fixtureDef.shape = circleShape;
        body.CreateFixture(fixtureDef);
        body.SetGravityScale(0);
        body.SetLinearDamping(2.0);
        body.SetAngularDamping(2.0);

        //body.SetUserData(this);
    }

    private const moveSpeed:Number = 25.0;
    private const rotationSpeed:Number = 500;
    override public function update(deltaTime:Number):void
    {
        if(Input.keyDown(Keyboard.E)||Input.keyDown(Keyboard.Q))
        {
            ApplyTorque((Input.keyDown(Keyboard.E)?1.0:-1.0)*Deg2Rad(rotationSpeed)*body.GetInertia());
        }
        var linearVelocity:b2Vec2;
        if(Input.keyDown(Keyboard.NUMPAD_SUBTRACT))
        {
            linearVelocity = body.GetLinearVelocity().Copy();
            Portal.ScaleBody(body,0.95);
            body.SetLinearVelocity(linearVelocity);
        }
        else if(Input.keyDown(Keyboard.NUMPAD_ADD))
        {
            linearVelocity = body.GetLinearVelocity().Copy();
            Portal.ScaleBody(body,1.05);
            body.SetLinearVelocity(linearVelocity);
        }

        var fwd:Point2D = new Point2D(1.0,0.0).Rotate(rot);
        var up:Point2D = new Point2D(0.0,-1.0).Rotate(rot);

        if(Input.keyDown(Keyboard.RIGHT)||Input.keyDown(Keyboard.D))
        {
            ApplyForce(fwd.Times(moveSpeed*scale*body.GetMass()).vec, body.GetWorldCenter());
        }
        if(Input.keyDown(Keyboard.LEFT)||Input.keyDown(Keyboard.A))
        {
            ApplyForce(fwd.Times(-moveSpeed*scale*body.GetMass()).vec, body.GetWorldCenter());
        }
        if(Input.keyDown(Keyboard.UP)||Input.keyDown(Keyboard.W))
        {
            ApplyForce(up.Times(moveSpeed*scale*body.GetMass()).vec, body.GetWorldCenter());
        }
        if(Input.keyDown(Keyboard.DOWN)||Input.keyDown(Keyboard.S))
        {
            ApplyForce(up.Times(-moveSpeed*scale*body.GetMass()).vec, body.GetWorldCenter());
        }
    }
}
}
