/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 5/31/13
 * Time: 7:19 PM
 * To change this template use File | Settings | File Templates.
 */
package Game2D.Functions {
    public function Lerp(a:Number, b:Number, alpha:Number):Number {
        return a+(b-a)*alpha;
    }
}
