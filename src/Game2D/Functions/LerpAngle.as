/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 6/7/13
 * Time: 12:38 PM
 */
package Game2D.Functions {
public function LerpAngle(a:Number, b:Number, alpha:Number):Number {
    var angleDiff:Number = NormalizeAngle(b - a);
    const pi2:Number = Math.PI*2;
    if(angleDiff>Math.PI)
        angleDiff-=pi2;
    return a+angleDiff*alpha;
}
}
