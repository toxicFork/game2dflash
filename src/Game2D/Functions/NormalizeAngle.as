/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 6/2/13
 * Time: 8:01 PM
 */
package Game2D.Functions {
    public function NormalizeAngle(angle:Number):Number {
        const pi2:Number = Math.PI*2;
        angle = angle%pi2;
        if(angle<0)
            angle+=pi2;
        return angle;
    }
}
