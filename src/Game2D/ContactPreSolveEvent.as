/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 6/2/13
 * Time: 2:20 PM
 * To change this template use File | Settings | File Templates.
 */
package Game2D {
import Box2D.Collision.b2Manifold;
import Box2D.Dynamics.Contacts.b2Contact;
import Box2D.Dynamics.b2Fixture;

public class ContactPreSolveEvent extends ContactEvent{
    public static const PRE_SOLVE:String = "PRE_SOLVE";
    public var oldManifold:b2Manifold;


    public function ContactPreSolveEvent(type:String, contact:b2Contact, myFixture:b2Fixture, otherFixture:b2Fixture, oldManifold:b2Manifold) {
        super(type, contact, myFixture, otherFixture);
        this.oldManifold = oldManifold;
    }
}
}
