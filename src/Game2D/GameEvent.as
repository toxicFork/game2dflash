/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 6/2/13
 * Time: 1:14 PM
 * To change this template use File | Settings | File Templates.
 */
package Game2D {
import flash.events.Event;

public class GameEvent extends Event{
    public static const START:String = "START";
    internal static const INITIALISE:String = "INITIALISE";
    public function GameEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
    }
}
}
