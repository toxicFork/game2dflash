/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 5/29/13
 * Time: 4:49 AM
 * To change this template use File | Settings | File Templates.
 */
package Game2D {
import Box2D.Collision.b2Manifold;
import Box2D.Dynamics.Contacts.b2Contact;
import Box2D.Dynamics.b2Body;
import Box2D.Dynamics.b2ContactImpulse;
import Box2D.Dynamics.b2ContactListener;

public class ContactListener extends b2ContactListener {
    public function ContactListener() {
        super();
    }

    override public function BeginContact(contact:b2Contact):void
    {
        var bodyA:b2Body = contact.GetFixtureA().GetBody();
        var bodyB:b2Body = contact.GetFixtureB().GetBody();
        if(bodyA.GetUserData())
        {
            var objectA:WorldObject = bodyA.GetUserData() as WorldObject;
            if(objectA)
            {
                objectA.dispatchEvent(new ContactEvent(ContactEvent.BEGIN,contact, contact.GetFixtureA(), contact.GetFixtureB()));
            }
        }
        if(bodyB.GetUserData())
        {
            var objectB:WorldObject = bodyB.GetUserData() as WorldObject;
            if(objectB)
            {
                objectB.dispatchEvent(new ContactEvent(ContactEvent.BEGIN, contact, contact.GetFixtureB(), contact.GetFixtureA()));
            }
        }
    }

    override public function EndContact(contact:b2Contact):void
    {
        var bodyA:b2Body = contact.GetFixtureA().GetBody();
        var bodyB:b2Body = contact.GetFixtureB().GetBody();
        if(bodyA.GetUserData())
        {
            var objectA:WorldObject = bodyA.GetUserData() as WorldObject;
            if(objectA)
            {
                objectA.dispatchEvent(new ContactEvent(ContactEvent.END,contact, contact.GetFixtureA(), contact.GetFixtureB()));
            }
        }
        if(bodyB.GetUserData())
        {
            var objectB:WorldObject = bodyB.GetUserData() as WorldObject;
            if(objectB)
            {
                objectB.dispatchEvent(new ContactEvent(ContactEvent.END, contact, contact.GetFixtureB(), contact.GetFixtureA()));
            }
        }
    }

    override public function PreSolve(contact:b2Contact,oldManifold:b2Manifold):void
    {
        var bodyA:b2Body = contact.GetFixtureA().GetBody();
        var bodyB:b2Body = contact.GetFixtureB().GetBody();
        if(bodyA.GetUserData())
        {
            var objectA:WorldObject = bodyA.GetUserData() as WorldObject;
            if(objectA)
            {
                objectA.dispatchEvent(new ContactPreSolveEvent(ContactPreSolveEvent.PRE_SOLVE,contact, contact.GetFixtureA(), contact.GetFixtureB(), oldManifold));
            }
        }
        if(bodyB.GetUserData())
        {
            var objectB:WorldObject = bodyB.GetUserData() as WorldObject;
            if(objectB)
            {
                objectB.dispatchEvent(new ContactPreSolveEvent(ContactPreSolveEvent.PRE_SOLVE,contact, contact.GetFixtureB(), contact.GetFixtureA(), oldManifold));
            }
        }
    }

    override public function PostSolve(contact:b2Contact,impulse:b2ContactImpulse):void
    {
        var bodyA:b2Body = contact.GetFixtureA().GetBody();
        var bodyB:b2Body = contact.GetFixtureB().GetBody();
        if(bodyA.GetUserData())
        {
            var objectA:WorldObject = bodyA.GetUserData() as WorldObject;
            if(objectA)
            {
                objectA.dispatchEvent(new ContactPostSolveEvent(ContactPostSolveEvent.POST_SOLVE,contact, contact.GetFixtureA(), contact.GetFixtureB(), impulse));
            }
        }
        if(bodyB.GetUserData())
        {
            var objectB:WorldObject = bodyB.GetUserData() as WorldObject;
            if(objectB)
            {
                objectB.dispatchEvent(new ContactPostSolveEvent(ContactPostSolveEvent.POST_SOLVE,contact, contact.GetFixtureB(), contact.GetFixtureA(), impulse));
            }
        }
    }
}
}
