/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 6/2/13
 * Time: 2:11 PM
 * To change this template use File | Settings | File Templates.
 */
package Game2D {
import Box2D.Dynamics.Contacts.b2Contact;
import Box2D.Dynamics.b2Fixture;

import flash.events.Event;

public class ContactEvent extends Event {
    public static const BEGIN:String = "BEGIN";
    public static const END:String = "END";
    public var contact:b2Contact;
    public var myFixture:b2Fixture;
    public var otherFixture:b2Fixture;
    public function ContactEvent(type:String,contact:b2Contact, myFixture:b2Fixture, otherFixture:b2Fixture) {
        this.contact = contact;
        this.myFixture = myFixture;
        this.otherFixture = otherFixture;
        super(type, false, true);
    }
}
}
