/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 5/29/13
 * Time: 5:23 AM
 * To change this template use File | Settings | File Templates.
 */
package Game2D {
import flash.display.Stage;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.utils.Dictionary;

public class Input {
    private static var _mouseDown:Boolean;
    private static var keys:Dictionary = new Dictionary();
    public function Input() {
    }

    public static function keyDown(n:uint):Boolean
    {
        return keys[n];
    }

    public static function get mouseDown():Boolean {
        return _mouseDown;
    }

    public static function RegisterEvents(stage:Stage):void {
        stage.addEventListener(MouseEvent.MOUSE_DOWN,mousePress);
        stage.addEventListener(MouseEvent.MOUSE_UP,mouseRelease);
        stage.addEventListener(KeyboardEvent.KEY_DOWN, keyPressed);
        stage.addEventListener(KeyboardEvent.KEY_UP, keyReleased);
    }

    private static function keyReleased(event:KeyboardEvent):void {
        keys[event.keyCode]=false;
    }

    private static function keyPressed(event:KeyboardEvent):void {
        keys[event.keyCode]=true;
    }

    private static function mouseRelease(event:MouseEvent):void {
        _mouseDown = false;
    }

    private static function mousePress(event:MouseEvent):void {
        _mouseDown = true;
    }
}
}
