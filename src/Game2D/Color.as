/**
 * Created with IntelliJ IDEA.
 * User: $
 * Date: 18/05/13
 * Time: 18:20
 * To change this template use File | Settings | File Templates.
 */
package Game2D {
public class Color {
    private var _r:Number, _g:Number, _b:Number;

    public static const
            Red:uint = new Color(1,0,0).val,
            Green:uint = new Color(0,1,0).val,
            Blue:uint = new Color(0,0,1).val,
            Black:uint = new Color(0, 0, 0).val,
            White:uint = new Color(1,1,1).val;

    public function Color(r:Number, g:Number, b:Number) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    private static function clamp(val:int):int {
        return Math.max(Math.min(1,val),0);
    }

    public function get r():Number {
        return _r;
    }

    public function set r(value:Number):void {
        _r = clamp(value);
    }

    public function get g():Number {
        return _g;
    }

    public function set g(value:Number):void {
        _g = clamp(value);
    }

    public function get b():Number {
        return _b;
    }

    public function set b(value:Number):void {
        _b = clamp(value);
    }

    public function get val():uint {
        var r:uint = this.r*255, g:uint = this.g*255, b:uint = this.b*255;
        return r<<16|g<<8|b;
    }
}
}
