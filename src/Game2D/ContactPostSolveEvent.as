/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 6/2/13
 * Time: 2:23 PM
 * To change this template use File | Settings | File Templates.
 */
package Game2D {
import Box2D.Dynamics.Contacts.b2Contact;
import Box2D.Dynamics.b2ContactImpulse;
import Box2D.Dynamics.b2Fixture;

public class ContactPostSolveEvent extends ContactEvent{
    public var impulse:b2ContactImpulse;
    public static const POST_SOLVE:String = "POST_SOLVE";
    public function ContactPostSolveEvent(type:String, contact:b2Contact, myFixture:b2Fixture, otherFixture:b2Fixture, impulse:b2ContactImpulse) {
        this.impulse = impulse;
        super(type, contact, myFixture, otherFixture);
    }
}
}
