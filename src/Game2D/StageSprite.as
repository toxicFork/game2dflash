/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 5/27/13
 * Time: 11:32 AM
 * To change this template use File | Settings | File Templates.
 */
package Game2D {
import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.Event;

public class StageSprite extends Sprite {
    public function StageSprite() {
        if(stage)
        {
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;
            dispatchEvent(new GameEvent(GameEvent.INITIALISE));
        }
        else{
            addEventListener(Event.ADDED_TO_STAGE,init);
        }
    }

    private function init(event:Event=null):void {
        dispatchEvent(new GameEvent(GameEvent.INITIALISE));
    }
}
}
