/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 5/28/13
 * Time: 9:09 PM
 * To change this template use File | Settings | File Templates.
 */
package Game2D {
import Box2D.Collision.Shapes.b2PolygonShape;
import Box2D.Dynamics.b2Body;
import Box2D.Dynamics.b2Fixture;
import Box2D.Dynamics.b2World;

import flash.display.BitmapData;
import flash.geom.Matrix;
import flash.utils.Dictionary;

public class View {
    public var position:Point2D;
    public var rect:Rect;
    public var rotation:Number = 0;
    public var scale:Number = 1;
    public var matrix:Matrix;
    public var invertedMatrix:Matrix;
    public function View() {
    }

    public function clone():View{
        var other:View = new View();
        other.position = position.Clone();
        other.rect = rect.clone();
        other.rotation = rotation;
        other.scale = scale;
        other.matrix = matrix.clone();
        other.invertedMatrix = invertedMatrix.clone();
        return other;
    }


    public function RenderInto(world:b2World, bitmapData:BitmapData, offset:Point2D = null):void {
        var displayMatrix:Matrix = matrix.clone();
        if(offset!==null)
        {
            displayMatrix.translate(offset.x, offset.y);
        }
        var viewShape:b2PolygonShape = new b2PolygonShape();

        viewShape.SetAsOrientedBox(rect.width * scale / 2, rect.height * scale / 2, rect.center.vec, rotation);
        var renderedBodies:Dictionary = new Dictionary();

        function callback(fixture:b2Fixture):Boolean {
            var body:b2Body = fixture.GetBody();
            if(renderedBodies[body]===undefined)
            {
                renderedBodies[body] = true;
                var worldObject:WorldObject = body.GetUserData() as WorldObject;
                if (worldObject) {
                    if(worldObject.image)
                    {
                        var finalScale:Point2D = worldObject.imageScale.Times(worldObject.scale*Game.physicsScale);
                        var mat:Matrix = new Matrix();
                        mat.scale(finalScale.x, finalScale.y);
                        mat.rotate(worldObject.rot);
                        var displayPos:Point2D = worldObject.pos.Times(Game.physicsScale).Add(worldObject.imageOffset.Scale(finalScale).Rotate(worldObject.rot));
                        mat.translate(displayPos.x, displayPos.y);
                        mat.concat(displayMatrix);

                        bitmapData.draw(worldObject.image,mat);
                    }
                }
            }
            return true;
        }

        world.QueryShape(callback, viewShape);
    }

    public function UpdateMatrices():void {
        var offset:Point2D = (position.Times(-Game.physicsScale));

        matrix = new Matrix();
        matrix.translate(offset.x, offset.y);
        matrix.rotate(-rotation);
        matrix.scale(1 / scale, 1 / scale);

        invertedMatrix = matrix.clone();
        invertedMatrix.invert();
    }
}
}
