/**
 * Created with IntelliJ IDEA.
 * User: $
 * Date: 18/05/13
 * Time: 21:03
 * To change this template use File | Settings | File Templates.
 */
package Game2D {
import flash.display.Graphics;

public class Rect implements Drawable{
    public var x:Number, y:Number, width:Number, height:Number;
    public var rotation:Number;

    public function clone():Rect {
        var other:Rect = new Rect(x, y, width, height, rotation);
        return other;
    }

    private const
            INSIDE:uint = 0,
            LEFT:uint= 1,
            RIGHT:uint= 2,
            BOTTOM:uint= 4,
            TOP:uint= 8;

    public function get left():Number {
        return x;
    }
    public function get right():Number {
        return x + width;
    }
    public function get top():Number {
        return y;
    }
    public function get bottom():Number {
        return y + height;
    }

    private function get topLeft():Point2D {
        return new Point2D(x, y);
    }

    private function get size():Point2D {
        return new Point2D(width, height);
    }

    public function Rect(x:Number = 0,y:Number = 0,width:Number = 0,height:Number = 0, rotation:Number = 0) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.rotation = rotation;
    }

    private function ComputeCode(p:Point2D):uint
    {
        var code:uint = INSIDE;
        if(p.x<left)
            code |= LEFT;
        else if(p.x>right)
            code |= RIGHT;
        if(p.y<top)
            code |= TOP;
        else if(p.y>bottom)
            code |= BOTTOM;
        return code;
    }


    public function IntersectLine(line:Line,out:Line=null):Boolean
    {
        line = line.Rotate(-rotation, center);
        var a:Point2D = line.a, b:Point2D = line.b;
        var code0:uint = ComputeCode(a);
        var code1:uint = ComputeCode(b);
        var accept:Boolean = false;

        while(true)
        {
            if((code0|code1)==0){
                accept = true;
                break;
            }
            else if((code0&code1)!=0){
                break;
            }
            else {
                var x:Number, y:Number;

                var codeOut:uint = code0?code0:code1;

                if((codeOut&TOP)!=0) {
                    x = a.x+(b.x- a.x)*(top-a.y)/(b.y- a.y);
                    y = top;
                } else if((codeOut&BOTTOM)!=0) {
                    x = a.x+(b.x- a.x)*(bottom- a.y)/(b.y- a.y);
                    y = bottom;
                } else if((codeOut&RIGHT)!=0) {
                    y = a.y + (b.y- a.y) * (right- a.x)/(b.x- a.x);
                    x = right;
                } else if((codeOut&LEFT)!=0) {
                    y = a.y + (b.y- a.y) * (left - a.x)/(b.x- a.x);
                    x = left;
                }

                if(codeOut==code0)
                {
                    a = new Point2D(x,y);
                    code0 = ComputeCode(a);
                }
                else{
                    b = new Point2D(x,y);
                    code1 = ComputeCode(b);
                }
            }
        }

        if(accept&&out!=null)
        {
            out.a = a.Rotate(rotation, center);
            out.b = b.Rotate(rotation, center);
        }

        return accept;
    }


    public function Draw(gfx:Graphics, scale:Number = 1):void {
        var tL:Point2D = topLeft.Times(scale).Rotate(rotation, center.Times(scale));
        var tR:Point2D = tL.Add(new Point2D(width*scale,0).Rotate(rotation));
        var bR:Point2D = tR.Add(new Point2D(0, height*scale).Rotate(rotation));
        var bL:Point2D = bR.Add(new Point2D(-width*scale,0).Rotate(rotation));

        gfx.moveTo(tL.x, tL.y);
        gfx.lineTo(tR.x, tR.y);
        gfx.lineTo(bR.x, bR.y);
        gfx.lineTo(bL.x, bL.y);
        gfx.lineTo(tL.x, tL.y);

        var drawCenter:Point2D = center.Times(scale);
        gfx.drawCircle(drawCenter.x, drawCenter.y, 5);
    }


    public function Times(n:Number):Rect {
        return new Rect(x*n,y*n,width*n,height*n);
    }

    public function Move(delta:Point2D):Rect {
        return new Rect(x+delta.x, y+delta.y, width, height);
    }

    public function get center():Point2D {
        return topLeft.Add(size.Times(0.5));
    }

    public function Rotate(rotation:Number):Rect
    {
        return new Rect(x, y, width, height, this.rotation + rotation);
    }

    public function Scale(scale:Number, aroundCenter:Boolean = false):Rect
    {
        if(!aroundCenter)
            return new Rect(x*scale, y*scale, width*scale, height*scale);

        var halfSize:Point2D = size.Times(0.5);
        halfSize = halfSize.Times(scale);
        var newTopLeft:Point2D = center.Subtract(halfSize);
        return new Rect(newTopLeft.x, newTopLeft.y, halfSize.x*2, halfSize.y * 2);
    }

    public function contains(point:Point2D):Boolean {
        var rotatedPoint:Point2D = point.Rotate(-rotation, center);
        return rotatedPoint.x>left&&rotatedPoint.x<right&&rotatedPoint.y>top&&rotatedPoint.y<bottom;
    }
}
}
