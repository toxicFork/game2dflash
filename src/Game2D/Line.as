/**
 * Created with IntelliJ IDEA.
 * User: $
 * Date: 18/05/13
 * Time: 19:17
 * To change this template use File | Settings | File Templates.
 */
package Game2D {
import flash.display.Graphics;

public class Line implements Drawable{
    public var a:Point2D, b:Point2D;
    public function Line(a:Point2D = null, b:Point2D = null) {
        if(a==null)
            a = new Point2D();
        if(b==null)
            b = new Point2D();
        this.a = a;
        this.b = b;
    }

    public function Draw(gfx:Graphics, scale:Number = 1):void {
        gfx.moveTo(a.x*scale, a.y*scale);
        gfx.lineTo(b.x*scale, b.y*scale);
    }

    public function Intersect(other:Line):Point2D{
        var
                x1:Number = a.x,
                y1:Number = a.y,
                x2:Number = b.x,
                y2:Number = b.y,
                x3:Number = other.a.x,
                y3:Number = other.a.y,
                x4:Number = other.b.x,
                y4:Number = other.b.y;

        var det:Number = ((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4));
        return new Point2D(((x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4))/det,
                ((x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4))/det);
    }

    public function Rotate(rot:Number, center:Point2D):Line {
        return new Line(a.Rotate(rot, center), b.Rotate(rot, center));
    }
}
}
