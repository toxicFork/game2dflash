/**
 * Created with IntelliJ IDEA.
 * User: $
 * Date: 18/05/13
 * Time: 18:12
 * To change this template use File | Settings | File Templates.
 */
package Game2D {
import Box2D.Common.Math.b2Vec2;

import flash.geom.Point;

public class Point2D{
    public var x:Number, y:Number;
    public function Point2D(x:Number=0,y:Number=0) {
        this.x = x;
        this.y = y;
    }

    public function Dot(other:Point2D):Number{
        return x * other.x + y * other.y;
    }

    public static function Dot(a:Point2D, b:Point2D):Number {
        return a.Dot(b);
    }

    public function Add(other:Point2D):Point2D {
        return new Point2D(x+other.x,y+other.y);
    }

    public function Rotate(rot:Number, center:Point2D = null):Point2D {
        if(center==null)
            return new Point2D(x*Math.cos(rot)-y*Math.sin(rot),y*Math.cos(rot)+x*Math.sin(rot));
        return Subtract(center).Rotate(rot).Add(center);
    }

    public function Subtract(other:Point2D):Point2D {
        return new Point2D(x-other.x,y-other.y);
    }

    public function get magnitude():Number {
        return Math.sqrt(x*x+y*y);
    }

    public function get normalized():Point2D {
        return new Point2D(x/magnitude,y/magnitude);
    }

    public function Times(n:Number):Point2D {
        return new Point2D(x*n,y*n);
    }

    public function Clone():Point2D {
        return new Point2D(x,y);
    }

    public function get vec():b2Vec2 {
        return new b2Vec2(x,y);
    }

    public function get tangentLeft():Point2D {
        //noinspection JSSuspiciousNameCombination
        return new Point2D(y,-x);
    }
    public function get tangentRight():Point2D {
        //noinspection JSSuspiciousNameCombination
        return new Point2D(-y,x);
    }

    public static function FromVec(vec:b2Vec2):Point2D {
        return new Point2D(vec.x, vec.y);
    }

    public static function Lerp(a:Point2D, b:Point2D, alpha:Number):Point2D {
        return a.Add(b.Subtract(a).Times(alpha));
    }

    public static function FromPoint(point:Point):Point2D {
        return new Point2D(point.x, point.y);
    }

    public function get angle():Number {
        return Math.atan2(y,x);
    }

    public function get point():Point {
        return new Point(x,y);
    }

    public function Distance(other:Point2D):Number {
        return Subtract(other).magnitude;
    }

    public function Scale(scale:Point2D):Point2D {
        return new Point2D(x*scale.x,y*scale.y);
    }
}
}
