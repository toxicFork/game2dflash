package Game2D {

import Box2D.Collision.Shapes.b2PolygonShape;
import Box2D.Collision.Shapes.b2Shape;
import Box2D.Collision.b2AABB;
import Box2D.Common.Math.b2Vec2;
import Box2D.Dynamics.Joints.b2MouseJoint;
import Box2D.Dynamics.Joints.b2MouseJointDef;
import Box2D.Dynamics.b2Body;
import Box2D.Dynamics.b2DebugDraw;
import Box2D.Dynamics.b2Fixture;
import Box2D.Dynamics.b2World;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.Event;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.utils.getTimer;

[SWF(width='640', height='480', frameRate="120")]
public class Game extends StageSprite {
    protected var view:View;

    protected var world:b2World;
    protected var drawSprite:Sprite;
    public static var physicsScale:Number = 30.0;

    private const velocityIterations:int = 10;
    private const positionIterations:int = 10;
    private var mouseJoint:b2MouseJoint;

    private var bitmapData:BitmapData;
    private var bitmap:Bitmap;
    protected var mouseCoords:Point2D;
    //private var rot:Number = 0.0;
    //private var scale:Number = 1.0;
    private var lastTime:Number;
    private var dbgDraw:b2DebugDraw;
    public static var debugSprite:Sprite;

    protected var debug:Boolean;

    protected var rendering:Boolean = true;

    private var displayMatrix:Matrix;
    private var invertedDisplayMatrix:Matrix;

    public function Game(debug:Boolean = true) {
        this.debug = debug;
        addEventListener(GameEvent.INITIALISE,initialiseWorld);
        super();
    }

    private function initialiseWorld(event:GameEvent):void {
        drawSprite = new Sprite();
        debugSprite = new Sprite();
        bitmapData = new BitmapData(stage.stageWidth,stage.stageHeight);
        bitmap = new Bitmap(bitmapData);
        addChild(bitmap);

        //var worldAABB:b2AABB = new b2AABB();
        //worldAABB.lowerBound.Set(-1000.0, -1000.0);
        //worldAABB.upperBound.Set(1000.0, 1000.0);
        // Define the gravity vector
        var gravity:b2Vec2 = new b2Vec2(0.0, 10.0);

        // Allow bodies to sleep
        var doSleep:Boolean = true;

        // Construct a world object
        world = new b2World(gravity, doSleep);
        //world.SetBroadPhase(new b2BroadPhase(worldAABB));
        world.SetWarmStarting(true);

        //if(debug)
        {
            // set debug draw
            dbgDraw = new b2DebugDraw();
            dbgDraw.SetSprite(drawSprite);
            dbgDraw.SetDrawScale(physicsScale);
            dbgDraw.SetFillAlpha(0.3);
            dbgDraw.SetLineThickness(1.0);
            dbgDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
            world.SetDebugDraw(dbgDraw);
        }

        world.SetContactListener(new ContactListener());

        view = new View();
        view.position = new Point2D(0, 0);
        view.rect = new Rect(0,0,stage.stageWidth,stage.stageHeight);
        view.rotation = 0.0;
        view.scale = 1.0;

        Input.RegisterEvents(stage);
        stage.addEventListener(Event.ENTER_FRAME,update);
        stage.addEventListener(Event.RESIZE,reSized);

        reSized(null);

        dispatchEvent(new GameEvent(GameEvent.START));
    }

    private function reSized(event:Event):void {
        const offset:Number = -2;
        view.rect = new Rect(-offset,-offset,stage.stageWidth-offset*2,stage.stageHeight-offset*2).Times(1/physicsScale);

        bitmapData = new BitmapData(stage.stageWidth, stage.stageHeight);
        bitmap.bitmapData = bitmapData;
    }

    private function update(event:Event):void {
        var curTime:Number = getTimer() / 1000.0;
        var deltaTime:Number = curTime - lastTime;

        // Update physics
        world.Step(deltaTime, velocityIterations, positionIterations);
        world.ClearForces();

        updateWorld(deltaTime);

        var worldObject:WorldObject;
        for (var body:b2Body = world.GetBodyList(); body; body = body.GetNext()) {
            worldObject = body.GetUserData() as WorldObject;
            if (worldObject) {
                worldObject.doUpdate(deltaTime);
            }
        }

        updateViewMatrix();

        bitmapData.fillRect(bitmapData.rect, 0);

        var center:Point2D = new Point2D(stage.stageWidth / 2, stage.stageHeight / 2);

        if (rendering)
            view.RenderInto(world, bitmapData, center);


        //for (var i:uint = 0; i < numObjects; i++) {
        //   objects[i].update(curTime - lastTime);
        // }

        if (debug) {
            dbgDraw.SetLineThickness(view.scale);
            // Render
            world.DrawDebugData();
        }
        else {
            drawSprite.graphics.clear();
        }


//        if(mouseCoords&&viewShape)
//        if(viewShape.TestPoint(world.GetGroundBody().GetTransform(), mouseCoords.Times(1/physicsScale).vec))
//        {
//            drawSprite.graphics.lineStyle(1,Color.Black);
//            drawSprite.graphics.moveTo(view.rect.center.x*physicsScale, view.rect.center.y*physicsScale);
//            var pos:Point2D = mouseCoords;
//            drawSprite.graphics.lineTo(pos.x, pos.y);
//        }

        for (body = world.GetBodyList(); body; body = body.GetNext()) {
            worldObject = body.GetUserData() as WorldObject;
            if (worldObject) {
                worldObject.render(drawSprite.graphics, view);
            }
        }


        //drawSprite.graphics.lineStyle(player.scale, Color.Black, 1);
        //drawSprite.graphics.drawCircle(player.pos.x * physicsScale, player.pos.y * physicsScale, 5.0 * player.scale);

        renderWorld();

        //for (var i = 0; i < numObjects; i++) {
        //    objects[i].render(drawSprite.graphics, view, physicsScale);
        //}

        bitmapData.draw(drawSprite, displayMatrix);
        bitmapData.draw(debugSprite, displayMatrix);

        //matrix.invert();
        mouseCoords = Point2D.FromPoint(invertedDisplayMatrix.transformPoint(new Point(bitmap.mouseX, bitmap.mouseY)));
        mouseDrag();

        lastTime = curTime;
    }

    private function updateViewMatrix():void {
        updateView();

        view.UpdateMatrices();

        var center:Point2D = new Point2D(stage.stageWidth/2,stage.stageHeight/2);

        displayMatrix = view.matrix.clone();
        displayMatrix.translate(center.x, center.y);
        invertedDisplayMatrix = displayMatrix.clone();
        invertedDisplayMatrix.invert();
    }

    //noinspection JSUnusedGlobalSymbols
    protected function convertCoords(coords:Point2D):Point2D {
        if (displayMatrix == null)
            updateViewMatrix();
        return Point2D.FromPoint(invertedDisplayMatrix.transformPoint(coords.point));
    }
    protected function renderWorld():void {}

    //noinspection JSUnusedLocalSymbols
    protected function updateWorld(deltaTime:Number):void {}

    protected function updateView():void {}

    private function mouseDrag():void {
        if(Input.mouseDown&&!mouseJoint)
        {
            var body:b2Body = getBodyAtMouse();
            if(body)
            {
                var md:b2MouseJointDef = new b2MouseJointDef();
                md.bodyA = world.GetGroundBody();
                md.bodyB = body;
                md.target.Set(mouseCoords.x/physicsScale,mouseCoords.y/physicsScale);
                md.collideConnected = true;
                md.maxForce = 300.0 * body.GetMass();
                mouseJoint = world.CreateJoint(md) as b2MouseJoint;
                body.SetAwake(true);
            }
        }
        if(!Input.mouseDown)
        {
            if(mouseJoint)
            {
                world.DestroyJoint(mouseJoint);
                mouseJoint = null;
            }
        }
        if(mouseJoint)
        {
            mouseJoint.SetTarget(new b2Vec2(mouseCoords.x/physicsScale,mouseCoords.y/physicsScale));
        }
    }

    public function getBodyAtMouse(includeStatic:Boolean = false):b2Body {
        // Make a small box.
        var mousePVec:b2Vec2 = new b2Vec2(mouseCoords.x/physicsScale, mouseCoords.y/physicsScale);
        var aabb:b2AABB = new b2AABB();
        aabb.lowerBound.Set(mousePVec.x - 0.001, mousePVec.y - 0.001);
        aabb.upperBound.Set(mousePVec.x + 0.001, mousePVec.y + 0.001);
        var body:b2Body = null;

        // Query the world for overlapping shapes.
        function GetBodyCallback(fixture:b2Fixture):Boolean
        {
            var shape:b2Shape = fixture.GetShape();
            if (!fixture.IsSensor() && (fixture.GetBody().GetType() != b2Body.b2_staticBody || includeStatic))
            {
                var inside:Boolean = shape.TestPoint(fixture.GetBody().GetTransform(), mousePVec);
                if (inside)
                {
                    body = fixture.GetBody();
                    return false;
                }
            }
            return true;
        }
        world.QueryAABB(GetBodyCallback, aabb);
        return body;
    }
}
}
