/**
 * Created with IntelliJ IDEA.
 * User: toxicfork
 * Date: 6/1/13
 * Time: 9:07 PM
 * To change this template use File | Settings | File Templates.
 */
package Game2D {
import Box2D.Common.Math.b2Vec2;
import Box2D.Dynamics.b2Body;

import flash.display.Graphics;
import flash.display.IBitmapDrawable;
import flash.events.EventDispatcher;
import flash.geom.Matrix;

public class WorldObject extends EventDispatcher {
    private var _body:b2Body;
    protected var enabled:Boolean;
    public var image:IBitmapDrawable;
    public var imageOffset:Point2D = new Point2D();
    public var imageScale:Point2D = new Point2D(1,1);
    public var matrix:Matrix = new Matrix();
    public var scale:Number;

    public function WorldObject(body:b2Body) {
        enabled = true;
        scale = 1.0;
        _body = body;
        _body.SetUserData(this);
    }

    internal function doUpdate(deltaTime:Number):void{
        matrix = new Matrix();
        matrix.rotate(rot);
        //trace(imageScale.x, imageScale.y);
        //matrix.scale(imageScale.x, imageScale.y);
        matrix.translate(pos.x-imageOffset.x, pos.y-imageOffset.y);
        update(deltaTime);
    }
    public function update(deltaTime:Number):void {}
    public function render(graphics:Graphics, mouseInfo:View):void {}

    public final function get rot():Number {
        return _body.GetAngle();
    }

    public final function set rot(val:Number):void {
        _body.SetAngle(val);
    }

    public final function get pos():Point2D
    {
        var p:b2Vec2 = _body.GetPosition();
        return new Point2D(p.x,p.y);
    }
    public final function set pos(pos:Point2D):void {
        _body.SetPosition(new b2Vec2(pos.x,pos.y));
    }

    public function get body():b2Body {
        return _body;
    }


    public final function Destroyed():void {
        enabled = false;
    }


    public function ApplyImpulse(impulse:b2Vec2, point:b2Vec2):void {
        body.ApplyImpulse(impulse, point);
    }

    public function SetAngularVelocity(vel:Number): void {
        if(!body.IsAwake())
            body.SetAwake(true);
        body.SetAngularVelocity(vel);
    }

    public function SetLinearVelocity(vel:Point2D):void {
        if(!body.IsAwake())
            body.SetAwake(true);
        body.SetLinearVelocity(vel.vec);
    }
}
}
