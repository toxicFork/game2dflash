/**
 * Created with IntelliJ IDEA.
 * User: $
 * Date: 18/05/13
 * Time: 19:18
 * To change this template use File | Settings | File Templates.
 */
package Game2D {
import flash.display.Graphics;

public interface Drawable {
    function Draw(gfx:Graphics, scale:Number = 1):void;
}
}
